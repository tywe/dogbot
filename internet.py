import os
import tweepy

def get_api():
    auth = tweepy.OAuthHandler(os.environ['consumer_key'], os.environ['consumer_secret'])
    auth.set_access_token(os.environ['access_token'], os.environ['access_token_secret'])
    return tweepy.API(auth)


def send_tweet(tweet):
    api = get_api()
    status = api.update_status(status=tweet)
    return status

def fav(id):
    api = get_api()
    status = api.create_favorite(id)
    return status


def get_tweet(id):
    api = get_api()
    status = api.get_status(id)
    return status