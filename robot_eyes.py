import io
import os
import json
import base64

# Imports the Google Cloud client library
from google.oauth2 import service_account
from google.cloud import vision

info = json.loads(base64.b64decode(os.environ['gcp_key']))
credentials = service_account.Credentials.from_service_account_info(info)

def dog_detector(uri):
    client = vision.ImageAnnotatorClient(credentials=credentials)

    request = {
        'image': {
            'source': {'image_uri': uri},
        },
     }

    response = client.annotate_image(request=request)
    
    labels = response.label_annotations

    safe_search_ratings = {
        'adult': response.safe_search_annotation.adult,
        'spoof': response.safe_search_annotation.spoof,
        'medical': response.safe_search_annotation.medical,
        'violence': response.safe_search_annotation.violence,
        'racy': response.safe_search_annotation.racy,
    }

    # 1 = VERY_UNLIKELY
    # 2 = UNLIKELY
    # 3 = POSSIBLE
    # 4 = LIKELY
    # 5 = VERY_LIKELY
    for category, rating in safe_search_ratings.items():
        if (
            (rating >= 4) or
            (category in ['adult', 'racy'] and rating >= 3) or
            (category in ['violence'] and rating >= 2)
        ):
            print(f"SafeSearch {category} rating is {rating}; Exiting")
            return False, 0

    for label in labels:
        if label.description == "Dog":
            rounded_score = round(label.score * 100, 6)
            print(f"Possible dog found! ({label.score})")
            if label.score >= 0.75:
                print(f"It's probably a dog! Woof {rounded_score}%")
                return True, rounded_score
            else:
                return False, rounded_score
    return False, 0