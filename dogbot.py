import json
import os
import random
from time import time
import tweepy
from robot_eyes import dog_detector
from feelings import sentiment
from internet import send_tweet, fav

tweets = []

blacklist = {
    # opt-out users, common false positives, cruelty, spam, etc
    'users': [
        "928198040061710336",
        "4167775273",
        "1101790659051048960",
        "2492411708",
    ],
    # Still have to use a list comprehension for this since the source is usually something like <a href=link_to_source>Name</a>
    'sources': [
        "Animal Army",
        "Change BZ",
        "Social Jukebox",
    ],
    # animal cruelty petitions are a good cause but not for this random happy feed
    'words': [
        "justice", 
        "change",
        "sign",
        "petition",
        "welfare",
        "abuse",
        "torturer",
        "cruel",
        "cruelty",
        "sale",
        "listing",
        "breeder",
    ]
}

class StdOutListener(tweepy.streaming.StreamListener):

    def __init__(self):
        self.start_time = time()
        self.time_limit = int(os.environ['time_limit'])

    def on_data(self, data):
        if (time() - self.start_time) < self.time_limit:
            tweets.append(json.loads(data))
            return True
        else:
            return False

    def on_error(self, status):
        print(status)


def get_stream():
    listen = StdOutListener()
    auth = tweepy.OAuthHandler(os.environ['consumer_key'], os.environ['consumer_secret'])
    auth.set_access_token(os.environ['access_token'], os.environ['access_token_secret'])
    return tweepy.Stream(auth, listen)


def safe_tweet(tweet):
    # Initial checks
    if (
        "text" in tweet                 and
        "media" in tweet["entities"]    and
        not tweet["possibly_sensitive"] and
        tweet["is_quote_status"] == False
    ):
        # no RTs/replies/quotes and make sure there's an image
        if (
            tweet["text"][:1] != "@"    and
            tweet["text"][:4] != "RT @" and
            tweet["entities"]["media"][0]["media_url_https"][-4:] == ".jpg"
        ):
            # blacklist checks
            tweet_words = tweet["text"].split(" ")
            if (
                tweet["user"]["id_str"] not in blacklist['users']           and
                not any(word in tweet_words for word in blacklist['words']) and
                not any(source in tweet["source"] for source in blacklist['sources'])
            ):
                print("got one!")
                return True
    return False


def see_spot():
    stream = get_stream()

    stream.filter(track=json.loads(os.environ['filter_list']))

    processed_tweet_count = 0

    for tweet in tweets:
        if safe_tweet(tweet):
            processed_tweet_count += 1
            tweet_url = f"https://twitter.com/{tweet['user']['screen_name']}/status/{tweet['id']}"
            media_url = tweet["entities"]["media"][0]["media_url_https"]

            is_dog, score = dog_detector(media_url)

            print(tweet_url)
            print(tweet)
            print(score)
            
            if is_dog:
                humanlike_sentiment = sentiment()
                new_tweet = f"*WHIRRRR-BOOP*\nDOG DETECTED WITH {score}% CERTAINTY!\n{humanlike_sentiment}\n{tweet_url}"
                fav(tweet["id"])
                send_tweet(new_tweet)
                print(f"Processed tweets: {processed_tweet_count}")
                return new_tweet

    print(f"Processed tweets: {processed_tweet_count}")
    return False


def handler(event, context):
    randomizer = random.randint(1, 6)
    if  randomizer > 1:
        print(f"Random number was {randomizer}, not this time")
    else:
        result = see_spot()
        print(result)

# if __name__ == "__main__":
#     handler("", "")