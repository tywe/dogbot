import random

# Not proud of using numeric strings here BUT
# - can't figure out descriptive keys that still don't lean numeric anyway (e.g. adjectives_1)
# - numbers actually kinda make the mad libs cleaner to read
# - list order as a key would be easier to mess up, and order is critical here
# - could just use ints instead of strings, but keeping it consistent is ok for now 
#   (and won't make me confused about if this is a dict or list at a glance later)
options = {
    '1': [
        "pup",
        "doggo",
        "dog",
        "canine",
        "pupper",
        "friendly friend",
        "ball of floof",
    ],
    '2': [
        "all the",
        "so many",
        "loads and loads of",
        "all your",
    ],
    '3': [
        "scratches",
        "tummy rubs",
        "pets",
        "hugs",
        "zoomies",
        "snuggles",
        "cuddles",
    ],
    '4': [
        "squee",
        "awww",
        "love it",
        "adorbs",
        "so cute",
        "wow",
    ],
    '5': [
        "a sweet",
        "an adorable",
        "a lovable",
        "a floofy",
        "a fluffy",
        "a cuddly",
        "a good",
    ],
    '6': [
        "sweet",
        "adorable",
        "lovable",
        "floofy",
        "fluffy",
        "cuddly",
        "good",
    ],
    '7': [
        "pet",
        "play fetch with",
        "play ball with",
        "run with",
        "sit in the sun with",
        "smooch",
        "scratch the ears of",
        "scratch the tummy of",
        "scritch and scratch",
    ],
    '8': [
        "just want to",
        "wish you could",
        "wanna",
        "feel like taking the rest of the day off to",
        "feel like taking the rest of the week off to",
        "feel like abandoning all your human tasks to",
    ],
    '9': [
        "such",
        "those",
        "love those",
    ],
    '10': [
        "lovely",
        "adorable",
        "amazing",
        "cuddly",
        "stunning",
    ],
    '11': [
        "eyes",
        "ears",
        "paws",
        "dog features",
        "pup features",
        "canine features",
        "genetics",
    ],
    'e': [
        "❤️",
        "💚",
        "💙",
        "💜",
        "🐶",
        "😍",
    ],
}

def deserves():
    return f"this {filler('1')} deserves {filler('2')} {filler('3')}"


def singletons():
    return f"{filler('4')} {filler('e')}"


def what():
    return f"what {filler('5')} {filler('1')}"


def sooooo():
    return f"sooooo {filler('6')}"


def looks():
    return f"this {filler('1')} looks so {filler('6')}"


def dont():
    return f"don't you {filler('8')} {filler('7')} this {filler('6')} {filler('1')}"


def features():
    return f"{filler('9')} {filler('10')} {filler('11')}"


def time():
    return f"time for {filler('3')}"


def oops_all_emoji():
    return f"{filler('e')} {filler('e')} {filler('e')} {filler('e')} {filler('e')} {filler('e')}"


possible_sentiments = [
    deserves,
    singletons,
    what,
    sooooo,
    looks,
    dont,
    features,
    time,
    oops_all_emoji,
]


def sentiment():
    sentiment = random.choice(possible_sentiments)()
    return sentiment.upper()

def filler(option):
    return random.choice(options[option])

if __name__ == "__main__":
    print(sentiment())