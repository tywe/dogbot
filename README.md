# About

https://tywe.gitlab.io/dogbot/

ROBOT THAT LOVES ALL YOUR DOGS. TRIES TO TWEET EVERY DAY OR SO.

Made with 💚 by [Tyler](https://gitlab.com/tywe). DM the bot for removals/opt-out.

# Technical info

Compatible with Python 3.7. The code runs on AWS Lambda, and it uses the Google Vision API to detect if a picture is likely a dog. MULTI-CLOUD, BABY

# To do

* Fine-tune run schedule
    * 1000 free calls to vision api/month = about 3 runs per day, but some might be all misses. Trying at least a positive hit per day.
    * $1.50 per 1000 calls after that, so cheap to go over by a bit